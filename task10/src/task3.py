import csv


def get_top_performers(file_path, number_of_top_students=5):
    performers = sorted(get_performers(file_path), key=lambda item: float(item[2]), reverse=True)
    return list(map(lambda item: item[0], performers))[:number_of_top_students]


def get_performers(file_path):
    try:
        with open(file_path, newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            header = next(csvreader)
            performers = list()

            for row in csvreader:
                performers.append(row)
    finally:
        csvfile.close()
        return performers


def write_performers_age(file_path, file_path_to_write='../data/performers_age.csv'):
    try:
        with open(file_path_to_write, 'w') as csvfile:
            fieldnames = ['student name', 'age', 'average mark']
            csvwriter = csv.writer(csvfile, delimiter=',')
            csvwriter.writerow(fieldnames)
            performers = sorted(get_performers(file_path), key=lambda item: int(item[1]), reverse=True)
            for row in performers:
                csvwriter.writerow(row)
    finally:
        csvfile.close()


if __name__ == '__main__':
    print(get_top_performers("../data/students.csv"))
    write_performers_age("../data/students.csv")
