if __name__ == '__main__':
    try:
        with open('../data/unsorted_names.txt', encoding='utf-8') as f:
            sorted_names = list()
            for i in f:
                sorted_names.append(i)

        sorted_names = sorted(sorted_names)

        with open('../data/sorted_names.txt', 'w', encoding='utf-8') as f:
            for i in sorted_names:
                f.write(i)
    finally:
        f.close()
