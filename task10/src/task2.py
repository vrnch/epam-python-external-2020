import string
import re


def most_common_words(filepath, number_of_words=3):
    """
    Find the most common words from the text
    """
    try:
        word_histogram = dict()
        words = list()

        with open(filepath, encoding='utf-8') as f:
            for i in f:
                words.extend(i.split())

        words = remove_punctuations(words)
        word_histogram = create_histogram(word_histogram, words)

    finally:
        f.close()
        return list(word_histogram.keys())[:number_of_words]



def create_histogram(word_histogram, words):
    """
    Create frequency histogram in descending order
    """
    word_histogram = {word: word_histogram.get(word, 0) + 1 for word in words}

    word_histogram = {k: v for k, v in sorted(
        word_histogram.items(), key=lambda item: item[1], reverse=True
    )}
    return word_histogram


def remove_punctuations(words):
    """
    Remove punctuations from string items
    """
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    return list(map(lambda word: regex.sub('', word), words))


if __name__ == '__main__':
    print(most_common_words('../data/lorem_ipsum.txt'))
