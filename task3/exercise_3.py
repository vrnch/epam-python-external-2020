CARD_POINTS = {
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 0,
    'J': 0,
    'Q': 0,
    'K': 0,
    'A': 1,
}


def play(first_card: str, second_card: str) -> str:
    if first_card not in CARD_POINTS or second_card not in CARD_POINTS:
        return 'Do not cheat!'

    if first_card in CARD_POINTS and second_card in CARD_POINTS:
        total = CARD_POINTS[first_card] + CARD_POINTS[second_card]
        if total > 9:
            total -= 10
    return str(total)


if __name__ == '__main__':
    while True:
        first_card = str(input('Play first card: '))
        if len(first_card) < 1:
            break
        second_card = str(input('Play second card: '))
        if len(second_card) < 1:
            break
        print(
            'Your result: ', play(first_card, second_card)
        )
        print()
