def fizzbuzz(number: int) -> str:
    if not _check_range(number):
        return f'{number} is out of range [1, 100]'

    if number % 3 == 0 and number % 5 == 0:
        return 'FizzBuzz'
    elif number % 3 == 0:
        return 'Fizz'
    elif number % 5 == 0:
        return 'Buzz'
    else:
        return str(number)


def _check_range(number):
    return 1 <= number <= 100


if __name__ == '__main__':
    while True:
        number = input('Enter the number: ')
        if len(number) < 1:
            break
        number = int(number)
        print(fizzbuzz(number))
