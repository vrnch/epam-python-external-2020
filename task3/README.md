# Exercise 1 [[solution]](./exercise_1.py)
- Write a program that determines whether the Point A(x, y) is in the shaded area or not.
  
![alt text](graph.png)  
  
- Snippet for the program:  
```python
def validate_point(x: float, y: float):
    pass


if __name__ == '__main__':
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print('Is point in shadow area: ', validate_point(x, y))
```
- Example:
```
Enter x: 0.5
Enter y: 0.7
Is point in shadow area: True
```

# Exercise 2 [[solution]](./exercise_2.py)
- Write a program that prints the input number from 1 to 100. But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz"  

```
Enter the number: 63
Fizz
Enter the number: 25
Buzz
Enter the number: 89
89
Enter the number: 45
FizzBuzz
Enter the number: 999
999 is out of range [1, 100]
```

# Exercise 3: Baccarat [[solution]](./exercise_3.py)
- Simulate the one round of play for baccarat game (https://en.wikipedia.org/wiki/Baccarat_(card_game))
- Rules:
  - cards have a point value:
    - the 2 through 9 cards in each suit are worth face value (in points);
    - the 10, jack, queen, and king have no point value (i.e. are worth zero);
    - aces are worth 1 point.
  - Sum the values of cards. If total is more than 9 reduce 10 from result. E.g. _5 + 9 = 10, 14 - 10 = 4, 4_ is the result.
  - Player is not allowed to play another cards like joker.
 - Examples:
```
Play first card: 10
Play second card: A
Your result: 1

Play first card: J
Play second card: 9
Your result: 9

Play first card: joker
Play second card: 5
Your result: Do not cheat!
```
- Skeleton:
```python
CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']


def play(first_card: str, second_card: str) -> str:
    pass


if __name__ == '__main__':
    first_card = str(input('Play first card: '))
    second_card = str(input('Play second card: '))

    print(
        'Your result: ', play(first_card, second_card)
    )
```