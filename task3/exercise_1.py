def validate_point(x: float, y: float):
    return True if -1 <= x <= 1 and 0 <= y <= 1 and y >= abs(x) else False


if __name__ == '__main__':
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print('Is point in shadow area: ', validate_point(x, y))
