# Task 1-6
Predict the output of the following code snippets.

![alt text](Capture.PNG)
### Output: 
If `a` can't be converted to `int`, `ValueError` will be raised.   
If `a` is less than `b`, `AssertionError` will be raised with the message `Not enough`.  

![alt text](Capture1.PNG)
### Output:
As the `bar()` function is not defined, the `NameError` will be raised.  
Then the `finally` block will be executed -- `after bar` will be printed.  
The rest of the code (after the `finally` block) will not be executed because the `try` block stops its execution after an exception raises.  

![alt text](Capture2.PNG)
### Output
`2` will be printed as in the `baz()` function the `finally` block is always executed. 

![alt text](Capture3.PNG)
### Output
`SyntaxError` will be raised as the `else` block has to follow the `except` block.  

![alt text](Capture4.PNG)
### Output 
`TypeError` will be raised as exceptions must derive from `BaseException`.  
And during handling of the above exception, `TypeError` will occur because catching classes that do not inherit from `BaseException` is not allowed.

![alt text](Capture5.PNG)
### Output  
If a correct filename is input then `FileNotFound` is not raised, and `Enter the filename` is prompted again.  
Otherwise, `FileNotFoundError` is handled by the `except` block -- `Input file not found` is printed.
