# Exercise 1: Transpose a matrix [[solution]](src/exercise_1.py)

* Write the script that can transpose any matrix.
* Note. The transpose of a matrix is an operator which flips a matrix over its diagonal; that is, it switches the row and column indices of the matrix A
by producing another matrix  
![alt text](matrix.png)
* Note: you could describe a matrix in Python using nested lists, e.g. _matrix = [[0, 1, 2], [3, 4, 5]]_
* Example:  
![alt text](example-matrix.png)

# Exercise 2: Factorial [[solution]](src/exercise_2.py)

* Calculate the factorial for positive number n

* Note. The factorial of a positive integer n is the product of all positive integers less than or equal to n:
![alt text](factorial-formula.png)

* Example:  
```
Please, enter the number: 8
Factorial of 8 is 48320
```

# Exercise 3: Fibonacci sequence [[solution]](src/exercise_3.py)
* For a positive integer _n_ calculate the result value, which is equal to the sum of the first n Fibonacci numbers.
* Note. Fibonacci numbers are a series of numbers in which each next number is equal to the sum of the two preceding ones:  

<div align="center">0, 1, 1, 2, 3, 5, 8, 13...  
(F(O) = 0, F(1) = F(2) = 1, then F(n) = F(n-1) + F(n-2) for n>2)</div>    

* Examples:
```
Please, enter the length of sequence: 10
Fibonacci sequence is [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
Sum of elements in Fibonacci sequence is 88
```

# Exercise 4: Binary representation [[solution]](src/exercise_4.py)

* For a positive integer _n_ calculate the result value, which is equal to the sum of the “1” in the binary representation of _n_.
* Note. Do not use any format functions
* Example:
```
Number is 1, binary representation is 1, sum is 1
Number is 2, binary representation is 10, sum is 1
Number is 22, binary representation is 10110, sum is 3
Number is 2S, binary representation is 11001, sum is 3
Number is 98, binary representation is 1100010, sum is 3
Number is 128, binary representation is 10000000, sum is 1
Number is 278, binary representation is 100010110, sum is 4
```