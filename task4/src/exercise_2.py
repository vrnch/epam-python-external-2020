def factorial_loop(number: int):
    if number < 0:
        return 'Factorial does not exist'

    factorial = 1
    for i in range(1, number + 1):
        factorial *= i

    return f'Factorial of {number} is {factorial}'


def factorial_recursion(number: int):
    if number < 0:
        return 'Factorial does not exist'

    if number >= 1:
        return number * factorial_recursion(number - 1)
    return 1


if __name__ == "__main__":
    number = int(input('Please, enter the number: '))

    print(factorial_loop(number))
    # print(factorial_recursion(number))
