def transpose_1(_matrix: list) -> list:
    number_of_columns = len(_matrix[0])
    number_of_rows = len(_matrix)

    transposed = [
        [_matrix[column][row] for column in range(number_of_rows)]
        for row in range(number_of_columns)
    ]
    return transposed


def transpose_2(_matrix: list) -> list:
    return [list(row) for row in zip(*_matrix)]


def transpose_3(_matrix: list) -> list:
    number_of_columns = len(_matrix[0])
    number_of_rows = len(_matrix)

    transposed = nullify(number_of_columns, number_of_rows)

    for row in range(number_of_columns):
        for column in range(number_of_rows):
            transposed[row][column] = _matrix[column][row]

    return transposed


def nullify(number_of_columns: int, number_of_rows: int) -> list:
    return [
        [None for column in range(number_of_rows)]
        for row in range(number_of_columns)
    ]


def print_matrix(_matrix: list):
    for row in _matrix:
        print(row)


if __name__ == "__main__":
    matrix = [[0, 1, 9], [9, 8, 5], [7, 3, 1], [3, 4, 0], [0, 1, 9], [9, 8, 5], [7, 3, 1], [3, 4, 0]]

    print('Original matrix is:')
    print_matrix(matrix)

    transposed = transpose_1(matrix)
    # transposed = transpose_2(matrix)
    # transposed = transpose_3(matrix)
    print('\nTransposed matrix is:')
    print_matrix(transposed)
