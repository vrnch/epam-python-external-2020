from typing import List


def fibonacci_loop(number: int) -> List[int]:
    result = list()
    if number == 1:
        result.append(0)
    else:
        result.extend([0, 1])
        for i in range(0, number - 2):
            result.append(result[i + 1] + result[i])
    return result


def fibonacci_recursion(number: int) -> List[int]:
    if number == 1:
        return [0]
    elif number == 2:
        return [0, 1]

    sequence = fibonacci_recursion(number - 1)
    sequence.append(sum([sequence[-2], sequence[-1]]))

    return sequence


if __name__ == '__main__':
    number = int(input('Please, enter the length of sequence: '))
    sequence = fibonacci_loop(number)
    # sequence = fibonacci_recursion(number)
    print(f'Fibonacci sequence is {sequence}')
