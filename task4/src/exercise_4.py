from typing import List


def to_binary(decimal: int):
    reminder = list()
    while decimal > 0:
        reminder.append(decimal % 2)
        decimal //= 2
    reminder.reverse()

    return reminder


def sum_binary(binary_numbers: List[int]):
    return sum([i for i in binary_numbers if i == 1])


if __name__ == '__main__':
    decimal = int(input('Enter decimal number: '))

    binary = to_binary(decimal)

    print(
        f'Number is {decimal}, '
        f'binary representation is {"".join([str(i) for i in binary])}, '
        f'sum is {sum_binary(binary)}'
    )
