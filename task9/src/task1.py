def replace_quote(string: str) -> str:
    if "'" in string:
        string = string.replace("'", '"')
    elif '"' in string:
        string = string.replace('"', "'")
    return string


if __name__ == "__main__":
    string = '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' \
             'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."'
    print('Original:', string)
    replaced = replace_quote(string)
    print('Replaced:', replaced)
    replaced_again = replace_quote(replaced)
    print('Replaced again:', replaced_again)
