from string import ascii_lowercase
import itertools


def test_1_1(*string) -> set:
    """
    characters that appear in all strings
    """
    if len(string) == 0:
        return set()

    common_symbols = set(string[0])
    for word in string:
        common_symbols = set(word).intersection(common_symbols)

    return common_symbols


def test_1_2(*string) -> set:
    """
    characters that appear in at least one string
    """
    all_symbols = set()
    for word in string:
        all_symbols = set(word).union(all_symbols)

    return all_symbols


def test_1_3(*string) -> set:
    """
    characters that appear at least in two strings
    """
    all_symbols = list()
    for word1, word2 in itertools.combinations(string, 2):
        all_symbols.extend(set(word1).intersection(word2))

    return set(all_symbols)


def test_1_4(*string) -> set:
    """
    characters of alphabet, that were not used in any string
    """
    return set(ascii_lowercase) - test_1_2(*string)


if __name__ == '__main__':
    test_strings = ["hello", "world", "python"]
    print(test_1_1(*test_strings))
    print(test_1_2(*test_strings))
    print(test_1_3(*test_strings))
    print(test_1_4(*test_strings))
