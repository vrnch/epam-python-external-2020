def count_letters(string: str) -> dict:
    histogram = dict()
    for i in string:
        histogram[i] = histogram.get(i, 0) + 1
    return histogram


if __name__ == '__main__':
    print(count_letters("stringsample"))
