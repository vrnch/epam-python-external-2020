def ispalindrome(string: str) -> bool:
    return True if string[::-1].lower() == string.lower() else False


if __name__ == '__main__':
    string = input('Enter your string: ')
    print(
        f'The string "{string}"',
        'is' if ispalindrome(string) is True else 'isn\'t',
        'a palindrome'
    )
