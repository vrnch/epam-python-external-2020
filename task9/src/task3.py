import re


def get_shortest_word(s: str) -> str:
    return min(re.findall(r"[\S]+", string), key=len)


if __name__ == '__main__':
    string = 'Python is simple and effective!'
    print(get_shortest_word(string))
