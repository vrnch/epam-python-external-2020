# Exercise 1 [[solution]](./exercise_1.py)
- Return the count of negative numbers in next list [4, -9, 8, -11, 8]
- Note: do not use conditionals or loops 

```python
def count_negatives(numbers: list) -> int:
    return 0

if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(input_numbers)} negative numbers in list {input_numbers}')
```

# Exercise 2 [[solution]](./exercise_2.py)
- You have first 5 best tennis players according API rankings. Set the first-place player (at the front of the list) to last place and vice versa.
- players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitlova']
```python
def change_positions(players: list):
    # my code here
    print(players)

if __name__ == '__main__':
    change_positions(
        players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitlova']
    )
```

# Exercise 3 [[solution]](./exercise_3.py)
- Swap words "reasonable" and "unreasonable" in quote "The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself."
- Note: Do not use <string>.replace() function or similar.