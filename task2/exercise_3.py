def swap(sentence: str, word_1: str, word_2: str) -> str:
    sentence = sentence.split()
    index_1, index_2 = sentence.index(word_1), sentence.index(word_2)
    sentence[index_1], sentence[index_2] = sentence[index_2], sentence[index_1]
    return ' '.join(sentence)


if __name__ == '__main__':
    quote = 'The reasonable man adapt himself to the world; the ' \
            'unreasonable one persists in trying to adapt the world to himself.'
    print(f'Before swapping: "{quote}"')
    print(f'After swapping: "{swap(quote, "reasonable", "unreasonable")}"')
