# Task 1 [[solution]](task1.py)

Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers) and combines them into one dictionary.
Dict values should be summarized in case of identical keys
```python
def combine dicts(* args):

dict_1 = {'a’: 100, 'b': 200}

dict_2 = {'a’: 200, 'c’: 300}

dict_3 = {'a’: 300, ‘d': 100}

print(combine dicts(dict_1, dict_2)

>>> {'a': 300, 'b’: 200, 'c': 300}

print(combine dicts(dict_1, dict_2, dict_3)

>>> {'a': 600, 'b’: 200, 'c': 300, 'd’: 100}
```

# Task 2 [[solution]](task2.py)

To create generic type CustomList — the list of values, which has length that is extended when new elements are added to the list.

CustomList is a collection — the list of values of random type, its size changes dynamically and there is a possibility to index list elements. Indexation in
the list starts with 0.

Values of random type can be located in the list, it should be created empty and a set of original values should be specified. List length changes while
adding and removing elements. The elements can be added and removed using specific methods. List can be checked whether there is a
predetermined value in the list.

List indexing allows to perform the following operations based on indexes:

* To change and read values of existing elements by using indexer

* To receive index of predetermined value

* To remove value based on index

The list can be cleared, its length can be identified, its elements can be received in the form of linked list Item that is available via link to head.
Collection CustomList can be used in operator foreach and other constructions that are oriented to the presence of numerator in class.

The task has two levels of complexity: Low and Advanced.

Low level tasks require implementation of the following functionality:

* Creating of empty user list and the one based on elements set (the elements are stored in CustomList in form of unidirectional linked list
* Adding, removing elements

* Operations with elements by index

* Clearing the list, receiving its length

* Receiving link to linked elements list

Advanced level tasks require implementation of the following functionality:

* All completed tasks of Low level

* Generating exceptions, specified in xml-comments to class methods

* Receiving from numerator list for operator foreach
