class Node:
    """The Node contains data and a pointer to the next Node"""

    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        """Return Node object representation"""
        return repr(self.data)


class CustomList():
    def __init__(self):
        self.head = None

    def __repr__(self):
        """Return representations of the CustomList object"""
        current_node = self.head

        result = str()
        while current_node:
            result += repr(current_node.data) + ', '
            current_node = current_node.next
        return '[' + result + ']'

    def __iter__(self):
        """Allow to iterate through the CustomList object"""
        current_node = self.head
        while current_node:
            yield current_node.data
            current_node = current_node.next

    def __len__(self) -> int:
        """
        Return a length of the Customlist object
        :returns: A length of the list
        """
        current_node = self.head
        count = 0
        while current_node:
            count += 1
            current_node = current_node.next
        return count

    def __getitem__(self, index):
        """
        Get data by an index
        :param index: The index of an item
        :returns: The data of the item
        """
        if index > len(self) - 1:
            raise IndexError("The index is out of range")

        current_node = self.head
        for i in range(index):
            current_node = current_node.next
        return current_node.data

    def __setitem__(self, index, data):
        """
        Update item's data by the index

        :param index: The index of the item
        :return
        """
        if index > len(self) - 1:
            raise IndexError("The index is out of range")

        current_node = self.head
        for i in range(index):
            current_node = current_node.next
        current_node.data = data

    def clear(self):
        """
        Clear the list
        :return:
        """
        self.head = None

    def append(self, data):
        """
        Add new item to the end of the list
        :param data: The data of the item
        :return
        """
        new_node = Node(data)

        if self.head is None:
            self.head = new_node
            return

        last_node = self.head
        while last_node.next:
            last_node = last_node.next

        if last_node.next is None:
            last_node.next = new_node

    def prepend(self, data):
        """
        Add new item in the beginning of the list
        :param data: The data of the item
        :return
        """
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node

    def index(self, data):
        """
        To receive index of predetermined value
        :param data: The data if the item
        :return
        """
        if data not in self:
            raise ValueError('The value must be contained in the list')

        current_node = self.head
        index = 0
        while current_node.data != data:
            index += 1
            current_node = current_node.next
        return index

    def delete_by_value(self, data):
        """
        Remove the first occurence of a value
        :param data: The value
        :return
        """
        if data not in self:
            raise ValueError('The value must be contained in the list')

        current_node = self.head
        if current_node and current_node.data == data:
            self.head = current_node.next
            current_node = None
            return

        else:
            previous_node = None
            while current_node and current_node.data != data:
                previous_node = current_node
                current_node = current_node.next

            if current_node is None:
                return

            previous_node.next = current_node.next
            current_node = None

    def delete_by_index(self, index):
        """
        Remove an element by its index
        :param index: The index of the removable element
        :return
        """
        if index > len(self) - 1:
            raise IndexError('The index is out of range')

        current_node = self.head
        previous_node = None

        if index == 0:
            self.head = current_node
            current_node = None

        counter = 0

        while current_node and counter < index:
            previous_node = current_node
            current_node = current_node.next
            counter += 1

        if current_node is None:
            return

        previous_node.next = current_node.next
        current_node = None

    def exist(self, data):
        """
        Check whether there is a predetermined value in the list
        :param data: The predetermined value
        :returns: The fact of existence in boolean form
        """
        current_node = self.head
        while current_node:
            if current_node.data == data:
                return True
            current_node = current_node.next
        return False


if __name__ == '__main__':
    llist = CustomList()
    for i in range(7):
        llist.append(chr(ord('A') + i))

    print(llist)

    print('Length = ', len(llist))

    for i in range(3):
        llist.prepend(1 + i)

    print(llist)

    print('Length = ', len(llist))

    print('The index of the "A" element is', llist.index('A'))

    llist.delete_by_value(1)

    print(llist)

    llist.delete_by_index(1)

    print(llist)

    print('Does the value "E" exist in the list?', llist.exist('E'))
    print('Does the value "Z" exist in the list?', llist.exist('Z'))

    llist.clear()

    print(llist)



