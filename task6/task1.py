def combine_dicts(*args: dict):
    combined = dict()
    for i in args:
        combined.update(i)
    return combined


dict_1 = {'a': 100, 'b': 200}

dict_2 = {'a': 200, 'c': 300}

dict_3 = {'a': 300, 'd': 100}

print(combine_dicts(dict_1, dict_2, dict_1))
print(combine_dicts(dict_1, dict_2, dict_3))
