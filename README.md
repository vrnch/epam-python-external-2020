# epam-python-external-2020
This repository is meant to store descriptions and my solutions of the tasks provided by EPAM University at [[RD UA Py] Online Python External Program #1](https://www.training.epam.ua/#!/Training/2583?lang=en).  
 
# Contents 
* [Task 1: Introduction to Python](task1/README.md)
* [Task 2: Types and Values](task2/README.md)
* [Task 3: Conditionals](task3/README.md)
* [Task 4: Loops](task4/README.md)
* [Task 5: Functions](task5/README.md)
* [Task 6: Data Structures](task6/README.md)
* [Task 7: Classes](task7/README.md)
* [Task 8: Exceptions](task8/README.md)
* [Task 9: Strings and operations](task9/README.md)
* [Task 10: File Operations](task10/README.md)
* [Task 11: Modules, packages](task11/README.md)
* [Task 12: Linux](task12/README.md)