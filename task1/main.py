class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.p = (a + b + c) / 2
        self.area = (self.p * (self.p - self.a) * (self.p - self.b) * (self.p - self.c)) ** 0.5

    def get_area(self):
        return self.area


triangle = Triangle(4.5, 5.9, 9)
print(round(triangle.get_area(), 2))
