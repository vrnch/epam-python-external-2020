# Task 1 [[solution]](src/task1.py)
Look through filemodules/legb.py.  
1\) Find a way to call inner_function without moving it from inside of enclosed_function.    
2.1) Modify ONE LINE in inner_function to make it print variable 'a' from global scope.  
2.2) Modify ONE LINE in inner_function to make it print variable 'a' form enclosing function.

# Task 2 [[solution]](src/task2.py)
Implement a decorator remember_result which remembers last result of function it decorates and prints it before next call.
  
```python
@remember_result                
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result

sum_list("a", "b")
>>> "Last result = 'None'"
>>> "Current result = 'ab'"
sum_list("abc", "cde")
>>> "Last result = 'ab'" 
>>> "Current result = 'abccde'"
sum_list(3, 4, 5)
>>> "Last result = 'abccde'" 
>>> "Current result = '345'"
```

# Task 3 [[solution]](src/task3.py)

Implement a decorator call_once which runs a function or method once and caches the result. All consecutive calls to this function should return cached result no matter the arguments.  

```python
@call_once
def sum_of_numbers(a, b):
    return a + b

print(sum_of_numbers(13, 42))
>>> 55
print(sum_of_numbers(999, 100))
>>> 55
print(sum_of_numbers(134, 412))
>>> 55
print(sum_of_numbers(856, 232))
>>> 55
```

# Task 4*

Run the module modules/mod_a.py. Check its result. Explain why does this happen. Try to change x to a list [1,2,3]. Explain the result. Try to change import to from x import * where x - module names. Explain the result.

### Answer: 
After running the module `filemodules/mod_a.py` the output was `5` because:
1. The `x` variable with value `5` was imported from `mod_b`.
2. The `mod_c.x` variable with value `5` was imported from `mod_b`.

After changing the value of `x` variable to a list `[1, 2, 3]` a number `5` was printed again  because:
1. `mod_c` imported the variable `x` with value `[1, 2, 3]`.
2. In the `mod_b` the `x` was reassigned to a number `5`: `mod_c.x = 5`

After importing the modules through `from <module> import *` a number `5` was printed again  because:
1. `from mod_c import *` imported the `x = [1, 2, 3]` that didn't already belong to the `mod_c` module.
2. `from mod_b import *` imported the variable `mod_c.x` with value `5`
3. `print(mod_c.x)` printed the value of `x` variable from the module `mod_c` that was imported from the `mod_b` module, not `mod_c` module. 