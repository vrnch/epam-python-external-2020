def remember_result(func):
    last_result = None

    def wrapper(*args):
        nonlocal last_result
        print(f"Last result = '{last_result}'")
        last_result = func(*args)

    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result


if __name__ == "__main__":
    sum_list("a", "b")
    sum_list("abc", "cde")
    sum_list(3, 4, 5)
