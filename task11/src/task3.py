def call_once(func):
    has_run = False
    result = 0

    def wrapper(a, b):
        nonlocal has_run
        nonlocal result

        if not has_run:
            result = func(a, b)
            has_run = True
            return result
        else:
            return result

    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == "__main__":
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(134, 412))
    print(sum_of_numbers(856, 232))
