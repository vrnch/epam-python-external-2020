a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        # a = "I am local variable!"
        global a  # item 2.1
                    # item 2.2 if `global a` is commented
        print(a)

    inner_function()  # item 1


if __name__ == '__main__':
    enclosing_funcion()
