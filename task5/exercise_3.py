import functools
import operator


def mult_arithmetic_elements(a1: int, t: int, n: int):
    return functools.reduce(operator.mul, arithmetic_progression(a1, t, n))


def arithmetic_progression(a1: int, t: int, n: int):
    """
    a1 - Initial element
    t - step
    n - number of elements
    """
    counter = 0
    while counter < n:
        yield a1
        a1 += t
        counter += 1


if __name__ == '__main__':
    print(
        mult_arithmetic_elements(5, 3, 4)
    )
