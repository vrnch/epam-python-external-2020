import exercise_1 as ex_1


def transform(array: list, order: ex_1.Enum):
    if ex_1.is_sorted(array, order=order):
        return [index + value for index, value in enumerate(array)]
    return 'The array isn\'t sorted'


if __name__ == '__main__':
    asc = [i * 2 for i in range(10)]

    print(*asc, sep=', ')
    print('Ascending:', transform(asc, order=ex_1.SortOrder.ASC))
    print('Descending:', transform(asc, order=ex_1.SortOrder.DESC))

    print()

    desc = [i * -2 for i in range(10)]
    print(*desc, sep=', ')
    print('Ascending:', transform(desc, order=ex_1.SortOrder.ASC))
    print('Descending:', transform(desc, order=ex_1.SortOrder.DESC))
