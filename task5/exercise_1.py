from enum import Enum


class SortOrder(Enum):
    ASC = 0
    DESC = 1


def is_sorted(array, order: Enum):
    if order is SortOrder.ASC and (all(array[i] <= array[i + 1] for i in range(len(array) - 1))):
        return True
    elif order is SortOrder.DESC and (all(array[i] >= array[i + 1] for i in range(len(array) - 1))):
        return True
    else:
        return False


if __name__ == '__main__':
    asc = [i * 2 for i in range(10)]

    print(*asc, sep=', ')
    print('Ascending:', is_sorted(asc, order=SortOrder.ASC))
    print('Descending:', is_sorted(asc, order=SortOrder.DESC))

    print()

    desc = [i * -2 for i in range(10)]
    print(*desc, sep=', ')
    print('Ascending:', is_sorted(desc, order=SortOrder.ASC))
    print('Descending:', is_sorted(desc, order=SortOrder.DESC))
