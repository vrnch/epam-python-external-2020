class Rectangle:
    def __init__(self, a, b=5):
        self.__side_a = a
        self.__side_b = b

    def get_side_a(self):
        return self.__side_a

    def get_side_b(self):
        return self.__side_b

    def area(self):
        return self.__side_a * self.__side_b

    def perimeter(self):
        return (self.__side_a + self.__side_b) * 2

    def is_square(self):
        return True if self.__side_a == self.__side_b else False

    def replace_sides(self):
        self.__side_a, self.__side_b = self.__side_b, self.__side_a

    def __repr__(self):
        return f"<Rectangle side_a = {self.__side_a}, side_b = {self.__side_b}>"


class ArrayRectangles:
    def __init__(self, n, *args: Rectangle):
        self.__rectangle_array = list()
        self.__length = 0
        self.__max_length = n

        if args:
            for i in args:
                if self.__length >= self.__max_length:
                    raise Exception(
                        f"Too many rectangles! Max length is {self.__max_length}.")
                if isinstance(i, Rectangle):
                    self.__rectangle_array.append(i)
                    self.__length += 1

    def add_rectangle(self, item: Rectangle):
        if self.__length < self.__max_length:
            self.__rectangle_array.append(item)
            return True
        return False

    def number_max_area(self):
        index = 0

        for i, rectangle in enumerate(self.__rectangle_array):
            if rectangle.area() > self.__rectangle_array[index].area():
                index = i
        return index

    def number_min_perimiter(self):
        index = 0

        for i, rectangle in enumerate(self.__rectangle_array):
            if rectangle.perimeter() < self.__rectangle_array[index].perimeter():
                index = i
        return index

    def number_square(self):
        total = 0

        for rectangle in self.__rectangle_array:
            if rectangle.is_square():
                total += 1

        return total


if __name__ == '__main__':
    rect = Rectangle(3, 3)
    print(rect.is_square())

    rect = Rectangle(4, 9)
    print(rect)
    rect.replace_sides()
    print(rect)

    arr_rect = ArrayRectangles(10, Rectangle(1, 1), Rectangle(2, 1), Rectangle(1, 1), Rectangle(40, 20),
                               Rectangle(4, 5))
    print(arr_rect.number_max_area())
    print(arr_rect.number_min_perimiter())
    print(arr_rect.number_square())
