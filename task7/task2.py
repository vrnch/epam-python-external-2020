class Employee:
    def __init__(self, name, salary):
        self._name = name
        self._salary = salary
        self._bonus = 0

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        return self._name

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, salary):
        self._salary = salary

    @property
    def bonus(self):
        return self._bonus

    def set_bonus(self, bonus):
        self._bonus = bonus

    def to_pay(self):
        return self._salary + self._bonus

    def __repr__(self):
        return f"<Employee name = {self._name}, salary = {self._salary}, bonus = {self._bonus}>"


class SalesPerson(Employee):
    def __init__(self, name, salary, percent):
        super().__init__(name, salary)
        self._percent = percent

    def set_bonus(self, bonus):
        if self._percent > 200:
            self._bonus = bonus * 3
        elif self._percent > 100:
            self._bonus = bonus * 2
        else:
            self._bonus = bonus

    def __repr__(self):
        return f"<SalesPerson percent = {self._percent}, {super().__repr__()}>"


class Manager(Employee):
    def __init__(self, name, salary, client_amount):
        super().__init__(name, salary)
        self._quantity = client_amount

    def set_bonus(self, bonus):
        if self._quantity > 150:
            self._bonus = bonus * 1000
        elif self._quantity > 100:
            self._bonus = bonus * 500
        else:
            self._bonus = bonus

    def __repr__(self):
        return f"<Manager quantity = {self._quantity}, {super().__repr__()}>"


class Company:
    def __init__(self, *args: Employee):
        self._employees = list()

        if args:
            for arg in args:
                if isinstance(arg, Employee):
                    self._employees.append(arg)

    def give_everybody_bonus(self, company_bonus):
        for employee in self._employees:
            employee.set_bonus(company_bonus)

    def total_to_pay(self):
        return sum([employee.to_pay() for employee in self._employees])

    def name_max_salary(self):
        temp = 0
        name = str()

        for employee in self._employees:
            if employee.to_pay() > temp:
                temp = employee.to_pay()
                name = employee.name
        return name

    def __repr__(self):
        return '<Company:\n' + '\n'.join([repr(employee) for employee in self._employees]) + '>'


if __name__ == '__main__':
    sp = SalesPerson('bob', 900, 555)
    print(sp)
    sp.set_bonus(40)
    print(sp)

    m = Manager('john', 1000, 101)
    print(m)
    m.set_bonus(20)
    print(m)

    company = Company(sp, m)
    company.give_everybody_bonus(946521)
    print(company)
    print('Total to pay:', company.total_to_pay())
    print('Max salary:', company.name_max_salary())
