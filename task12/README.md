# Prerequisites:
Install Windows Subsytem for Linux if you are using Windows.
Please, create screenshots with Linux command output results in you hometasks directories.

# Task 1  
**1a**. Invoke `pwd` to see your current working directory (there should be your home directory).
- **Output**:
<pre><font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ pwd
/home/vrnch</pre>

**1b**. Collect output of these commands  
```
ls -l /  
ls  
ls ~  
ls -l  
ls -a
ls -la  
ls -lda ~
```  
Note differences between produced outputs. Describe (in few words) purposes of these commands.

- **Output**:
<div>
<pre>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls -l / # list information about the directories and the files in the root directory using a long listing format
total 2097240
lrwxrwxrwx   1 root root          7 Oct 15 16:55 <font color="#06989A"><b>bin</b></font> -&gt; <font color="#3465A4"><b>usr/bin</b></font>
drwxr-xr-x   4 root root       4096 Nov 11 09:58 <font color="#3465A4"><b>boot</b></font>
drwxrwxr-x   2 root root       4096 Oct 15 16:56 <font color="#3465A4"><b>cdrom</b></font>
drwxr-xr-x  21 root root       4440 Nov 11 11:36 <font color="#3465A4"><b>dev</b></font>
drwxr-xr-x 138 root root      12288 Nov 11 09:59 <font color="#3465A4"><b>etc</b></font>
drwxr-xr-x   3 root root       4096 Oct 15 16:57 <font color="#3465A4"><b>home</b></font>
lrwxrwxrwx   1 root root          7 Oct 15 16:55 <font color="#06989A"><b>lib</b></font> -&gt; <font color="#3465A4"><b>usr/lib</b></font>
lrwxrwxrwx   1 root root          9 Oct 15 16:55 <font color="#06989A"><b>lib32</b></font> -&gt; <font color="#3465A4"><b>usr/lib32</b></font>
lrwxrwxrwx   1 root root          9 Oct 15 16:55 <font color="#06989A"><b>lib64</b></font> -&gt; <font color="#3465A4"><b>usr/lib64</b></font>
lrwxrwxrwx   1 root root         10 Oct 15 16:55 <font color="#06989A"><b>libx32</b></font> -&gt; <font color="#3465A4"><b>usr/libx32</b></font>
drwx------   2 root root      16384 Oct 15 16:54 <font color="#3465A4"><b>lost+found</b></font>
drwxr-xr-x   3 root root       4096 Oct 15 20:47 <font color="#3465A4"><b>media</b></font>
drwxr-xr-x   2 root root       4096 Jul 31 19:27 <font color="#3465A4"><b>mnt</b></font>
drwxr-xr-x   5 root root       4096 Nov  5 15:23 <font color="#3465A4"><b>opt</b></font>
dr-xr-xr-x 352 root root          0 Nov 11 09:46 <font color="#3465A4"><b>proc</b></font>
drwx------   5 root root       4096 Nov  7 09:04 <font color="#3465A4"><b>root</b></font>
drwxr-xr-x  34 root root        980 Nov 11 09:59 <font color="#3465A4"><b>run</b></font>
lrwxrwxrwx   1 root root          8 Oct 15 16:55 <font color="#06989A"><b>sbin</b></font> -&gt; <font color="#3465A4"><b>usr/sbin</b></font>
drwxr-xr-x   8 root root       4096 Oct 15 17:06 <font color="#3465A4"><b>snap</b></font>
drwxr-xr-x   2 root root       4096 Jul 31 19:27 <font color="#3465A4"><b>srv</b></font>
-rw-------   1 root root 2147483648 Oct 15 16:55 swapfile
dr-xr-xr-x  13 root root          0 Nov 11 09:46 <font color="#3465A4"><b>sys</b></font>
drwxrwxrwt  29 root root      12288 Nov 11 13:12 <span style="background-color:#4E9A06"><font color="#2E3436">tmp</font></span>
drwxr-xr-x  14 root root       4096 Jul 31 19:28 <font color="#3465A4"><b>usr</b></font>
drwxr-xr-x  14 root root       4096 Jul 31 19:35 <font color="#3465A4"><b>var</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls # list the directories and the files in the current directory
 <font color="#3465A4"><b>Desktop</b></font>     java_error_in_PYCHARM_4056.log   <font color="#3465A4"><b>Pictures</b></font>          <font color="#3465A4"><b>Templates</b></font>
 <font color="#3465A4"><b>Documents</b></font>   java_error_in_PYCHARM_5215.log   <font color="#3465A4"><b>Public</b></font>            <font color="#3465A4"><b>Videos</b></font>
 <font color="#3465A4"><b>Downloads</b></font>   java_error_in_PYCHARM_5981.log   <font color="#3465A4"><b>PycharmProjects</b></font>  <font color="#3465A4"><b>&apos;VirtualBox VMs&apos;</b></font>
 <font color="#3465A4"><b>env</b></font>         <font color="#3465A4"><b>Music</b></font>                            <font color="#3465A4"><b>snap</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls ~ # list the directories and the files in the home directory 
 <font color="#3465A4"><b>Desktop</b></font>     java_error_in_PYCHARM_4056.log   <font color="#3465A4"><b>Pictures</b></font>          <font color="#3465A4"><b>Templates</b></font>
 <font color="#3465A4"><b>Documents</b></font>   java_error_in_PYCHARM_5215.log   <font color="#3465A4"><b>Public</b></font>            <font color="#3465A4"><b>Videos</b></font>
 <font color="#3465A4"><b>Downloads</b></font>   java_error_in_PYCHARM_5981.log   <font color="#3465A4"><b>PycharmProjects</b></font>  <font color="#3465A4"><b>&apos;VirtualBox VMs&apos;</b></font>
 <font color="#3465A4"><b>env</b></font>         <font color="#3465A4"><b>Music</b></font>                            <font color="#3465A4"><b>snap</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls -l # list information about the directories and the files in the current directory using a long listing format
total 564
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Desktop</b></font>
drwxr-xr-x  6 vrnch vrnch   4096 Oct 21 15:02  <font color="#3465A4"><b>Documents</b></font>
drwxr-xr-x 10 vrnch vrnch  12288 Nov 11 12:56  <font color="#3465A4"><b>Downloads</b></font>
drwxrwxr-x  5 vrnch vrnch   4096 Nov  7 19:34  <font color="#3465A4"><b>env</b></font>
-rw-rw-r--  1 vrnch vrnch 170208 Nov 10 18:14  java_error_in_PYCHARM_4056.log
-rw-rw-r--  1 vrnch vrnch 169697 Oct 20 10:32  java_error_in_PYCHARM_5215.log
-rw-rw-r--  1 vrnch vrnch 167430 Nov 10 10:28  java_error_in_PYCHARM_5981.log
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Music</b></font>
drwxr-xr-x  4 vrnch vrnch  12288 Nov 10 20:23  <font color="#3465A4"><b>Pictures</b></font>
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Public</b></font>
drwxrwxr-x  5 vrnch vrnch   4096 Oct 20 18:34  <font color="#3465A4"><b>PycharmProjects</b></font>
drwxr-xr-x  3 vrnch vrnch   4096 Oct 15 17:13  <font color="#3465A4"><b>snap</b></font>
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Templates</b></font>
drwxr-xr-x  3 vrnch vrnch   4096 Oct 15 17:18  <font color="#3465A4"><b>Videos</b></font>
drwx------  3 vrnch vrnch   4096 Oct 15 21:14 <font color="#3465A4"><b>&apos;VirtualBox VMs&apos;</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls -a # list the directories and the files in the current directory not ignoring hidden files (entries starting with .)
 <font color="#3465A4"><b>.</b></font>               .gitconfig                       .profile
 <font color="#3465A4"><b>..</b></font>              <font color="#3465A4"><b>.gnome</b></font>                           <font color="#3465A4"><b>Public</b></font>
 <font color="#3465A4"><b>.anydesk</b></font>        <font color="#3465A4"><b>.gnome2</b></font>                          <font color="#3465A4"><b>PycharmProjects</b></font>
 .bash_history   <font color="#3465A4"><b>.gnupg</b></font>                           <font color="#3465A4"><b>.shutter</b></font>
 .bash_logout    <font color="#3465A4"><b>.java</b></font>                            <font color="#3465A4"><b>snap</b></font>
 .bashrc         java_error_in_PYCHARM_4056.log   <font color="#3465A4"><b>.ssh</b></font>
 <font color="#3465A4"><b>.cache</b></font>          java_error_in_PYCHARM_5215.log   .sudo_as_admin_successful
 <font color="#3465A4"><b>.config</b></font>         java_error_in_PYCHARM_5981.log   <font color="#3465A4"><b>Templates</b></font>
 <font color="#3465A4"><b>.dbus</b></font>           <font color="#3465A4"><b>.local</b></font>                           <font color="#3465A4"><b>.thumbnails</b></font>
 <font color="#3465A4"><b>Desktop</b></font>         <font color="#3465A4"><b>.mozilla</b></font>                         <font color="#3465A4"><b>.thunderbird</b></font>
 <font color="#3465A4"><b>Documents</b></font>       <font color="#3465A4"><b>Music</b></font>                            <font color="#3465A4"><b>Videos</b></font>
 <font color="#3465A4"><b>Downloads</b></font>       .pam_environment                <font color="#3465A4"><b>&apos;VirtualBox VMs&apos;</b></font>
 <font color="#3465A4"><b>env</b></font>             <font color="#3465A4"><b>Pictures</b></font>                         <font color="#3465A4"><b>.vscode</b></font>
 <font color="#3465A4"><b>.gconf</b></font>          <font color="#3465A4"><b>.pki</b></font>                             <font color="#3465A4"><b>.zoom</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls -la # list information about the files of the current directory using a long listing format and not ignoring hidden files (entries starting with .)
total 676
drwxr-xr-x 32 vrnch vrnch   4096 Nov 11 13:11  <font color="#3465A4"><b>.</b></font>
drwxr-xr-x  3 root  root    4096 Oct 15 16:57  <font color="#3465A4"><b>..</b></font>
drwxr-xr-x  4 vrnch vrnch   4096 Nov  7 13:02  <font color="#3465A4"><b>.anydesk</b></font>
-rw-------  1 vrnch vrnch  11161 Nov 11 13:11  .bash_history
-rw-r--r--  1 vrnch vrnch    220 Oct 15 16:57  .bash_logout
-rw-r--r--  1 vrnch vrnch   3771 Oct 15 16:57  .bashrc
drwx------ 30 vrnch vrnch   4096 Nov  9 14:56  <font color="#3465A4"><b>.cache</b></font>
drwx------ 31 vrnch vrnch   4096 Nov 11 11:54  <font color="#3465A4"><b>.config</b></font>
drwx------  3 vrnch vrnch   4096 Oct 15 20:19  <font color="#3465A4"><b>.dbus</b></font>
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Desktop</b></font>
drwxr-xr-x  6 vrnch vrnch   4096 Oct 21 15:02  <font color="#3465A4"><b>Documents</b></font>
drwxr-xr-x 10 vrnch vrnch  12288 Nov 11 12:56  <font color="#3465A4"><b>Downloads</b></font>
drwxrwxr-x  5 vrnch vrnch   4096 Nov  7 19:34  <font color="#3465A4"><b>env</b></font>
drwx------  2 vrnch vrnch   4096 Nov  9 10:55  <font color="#3465A4"><b>.gconf</b></font>
-rw-rw-r--  1 vrnch vrnch    111 Oct 15 21:44  .gitconfig
drwx------  3 vrnch vrnch   4096 Oct 15 20:57  <font color="#3465A4"><b>.gnome</b></font>
drwx------  2 vrnch vrnch   4096 Oct 22 18:31  <font color="#3465A4"><b>.gnome2</b></font>
drwx------  3 vrnch vrnch   4096 Nov  5 15:23  <font color="#3465A4"><b>.gnupg</b></font>
drwxrwxr-x  4 vrnch vrnch   4096 Oct 15 21:41  <font color="#3465A4"><b>.java</b></font>
-rw-rw-r--  1 vrnch vrnch 170208 Nov 10 18:14  java_error_in_PYCHARM_4056.log
-rw-rw-r--  1 vrnch vrnch 169697 Oct 20 10:32  java_error_in_PYCHARM_5215.log
-rw-rw-r--  1 vrnch vrnch 167430 Nov 10 10:28  java_error_in_PYCHARM_5981.log
drwxr-xr-x  3 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>.local</b></font>
drwx------  5 vrnch vrnch   4096 Oct 15 17:13  <font color="#3465A4"><b>.mozilla</b></font>
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Music</b></font>
-rw-r--r--  1 vrnch vrnch    310 Nov 11 13:11  .pam_environment
drwxr-xr-x  4 vrnch vrnch  12288 Nov 10 20:23  <font color="#3465A4"><b>Pictures</b></font>
drwx------  3 vrnch vrnch   4096 Oct 15 20:56  <font color="#3465A4"><b>.pki</b></font>
-rw-r--r--  1 vrnch vrnch    807 Oct 15 16:57  .profile
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Public</b></font>
drwxrwxr-x  5 vrnch vrnch   4096 Oct 20 18:34  <font color="#3465A4"><b>PycharmProjects</b></font>
drwxrwxr-x  3 vrnch vrnch   4096 Oct 23 12:14  <font color="#3465A4"><b>.shutter</b></font>
drwxr-xr-x  3 vrnch vrnch   4096 Oct 15 17:13  <font color="#3465A4"><b>snap</b></font>
drwx------  2 vrnch vrnch   4096 Oct 15 19:28  <font color="#3465A4"><b>.ssh</b></font>
-rw-r--r--  1 vrnch vrnch      0 Oct 15 19:30  .sudo_as_admin_successful
drwxr-xr-x  2 vrnch vrnch   4096 Oct 15 17:05  <font color="#3465A4"><b>Templates</b></font>
drwx------  3 vrnch vrnch   4096 Oct 22 18:31  <font color="#3465A4"><b>.thumbnails</b></font>
drwx------  6 vrnch vrnch   4096 Oct 21 11:08  <font color="#3465A4"><b>.thunderbird</b></font>
drwxr-xr-x  3 vrnch vrnch   4096 Oct 15 17:18  <font color="#3465A4"><b>Videos</b></font>
drwx------  3 vrnch vrnch   4096 Oct 15 21:14 <font color="#3465A4"><b>&apos;VirtualBox VMs&apos;</b></font>
drwxrwxr-x  3 vrnch vrnch   4096 Oct 18 20:42  <font color="#3465A4"><b>.vscode</b></font>
drwx------  8 vrnch vrnch   4096 Nov  5 18:32  <font color="#3465A4"><b>.zoom</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ ls -lda ~ # list information about the home directory itself using a long listing format and not ignoring hidden files (entries starting with .)
drwxr-xr-x 32 vrnch vrnch 4096 Nov 11 13:11 <font color="#3465A4"><b>/home/vrnch</b></font>
</pre>
  </div>

  
**1c**. Execute and describe the following commands (store the output, if any):
```
mkdir test
cd test
pwd
touch test.txt
ls -l test.txt
mkdir test2
mv test.txt test2
cd test2
ls
mv test.txt test2.txt
ls
cp test2.txt ..
cd ..
ls
rm test2.txt
rmdir test2
```
- **Execution and description**: 
<pre><font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ mkdir test # create a `test` directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ cd test # change the current/working directory to the `test` directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ pwd # print name of the current/working directory
/home/vrnch/test
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ touch test.txt # create here `test.txt` file
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls -l test.txt # list info about the `test.txt` file using a long listing format
-rw-rw-r-- 1 vrnch vrnch 0 Nov 11 13:35 test.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ mkdir test2 # create here a `test2` directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ mv test.txt test2 # move the `test.txt` file to the `test2` directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ cd test2 # change the current/working directory to the `test2` directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test/test2</b></font>$ ls # list the current directory contents
test.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test/test2</b></font>$ mv test.txt test2.txt # rename the `test.txt` file to `test2.txt` file
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test/test2</b></font>$ ls # list the current directory contents
test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test/test2</b></font>$ cp test2.txt .. # copy the `test2.txt` file to the parent directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test/test2</b></font>$ cd .. # hcange the current/working directory to the parent directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls # list the current directory contents
<font color="#3465A4"><b>test2</b></font>  test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ rm test2.txt # remove the `test2.txt` file
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ rmdir test2 # remove the `test2` directory
rmdir: failed to remove &apos;test2&apos;: Directory not empty</pre>


**1d**. Execute and describe the difference
```
cat /etc/fstab
less /etc/fstab
more /etc/fstab
```

<pre><font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ cat /etc/fstab # concatenates the contents of the all listed files and prints all of them to the standard output
# /etc/fstab: static file system information.
#
# Use &apos;blkid&apos; to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# &lt;file system&gt; &lt;mount point&gt;   &lt;type&gt;  &lt;options&gt;       &lt;dump&gt;  &lt;pass&gt;
# / was on /dev/nvme0n1p2 during installation
UUID=e5dbca1f-5a1b-4483-ac13-c5dade9ced01 /               ext4    errors=remount-ro 0       1
# /boot/efi was on /dev/nvme0n1p1 during installation
UUID=C5D7-9A40  /boot/efi       vfat    umask=0077      0       1
/swapfile                                 none            swap    sw              0       0


<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ less /etc/fstab # displays the contents of the file one screen at a time for large files allowing backward as well as forward movement
# /etc/fstab: static file system information.
#
# Use &apos;blkid&apos; to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# &lt;file system&gt; &lt;mount point&gt;   &lt;type&gt;  &lt;options&gt;       &lt;dump&gt;  &lt;pass&gt;
# / was on /dev/nvme0n1p2 during installation
UUID=e5dbca1f-5a1b-4483-ac13-c5dade9ced01 /               ext4    errors=remount-ro 0       1
# /boot/efi was on /dev/nvme0n1p1 during installation
UUID=C5D7-9A40  /boot/efi       vfat    umask=0077      0       1
/swapfile                                 none            swap    sw              0       0
<span style="background-color:#FFFFFF"><font color="#300A24">(END)</font></span>

*pressed `q` button*

<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ more /etc/fstab # displays the contents of the file one screen at a time for large files
# /etc/fstab: static file system information.
#
# Use &apos;blkid&apos; to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# &lt;file system&gt; &lt;mount point&gt;   &lt;type&gt;  &lt;options&gt;       &lt;dump&gt;  &lt;pass&gt;
# / was on /dev/nvme0n1p2 during installation
UUID=e5dbca1f-5a1b-4483-ac13-c5dade9ced01 /               ext4    errors=remount
-ro 0       1
# /boot/efi was on /dev/nvme0n1p1 during installation
UUID=C5D7-9A40  /boot/efi       vfat    umask=0077      0       1
/swapfile                                 none            swap    sw            
  0       0

</pre>

**1e**. Look through `man` pages of the listed above commands.

<pre>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ man cat

CAT(1)                           User Commands                          CAT(1)

<b>NAME</b>
       cat - concatenate files and print on the standard output

<b>SYNOPSIS</b>
       <b>cat</b> [<u style="text-decoration-style:single">OPTION</u>]... [<u style="text-decoration-style:single">FILE</u>]...

<b>DESCRIPTION</b>
       Concatenate FILE(s) to standard output.

       With no FILE, or when FILE is -, read standard input.

       <b>-A</b>, <b>--show-all</b>
              equivalent to <b>-vET</b>

       <b>-b</b>, <b>--number-nonblank</b>
              number nonempty output lines, overrides <b>-n</b>

       <b>-e</b>     equivalent to <b>-vE</b>

       <b>-E</b>, <b>--show-ends</b>
              display $ at end of each line
<span style="background-color:#FFFFFF"><font color="#300A24"> Manual page cat(1) line 1/71 31% (press h for help or q to quit)</font></span>


<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ man less
LESS(1)                     General Commands Manual                    LESS(1)

<b>NAME</b>
       less - opposite of more

<b>SYNOPSIS</b>
       <b>less</b> <b>-?</b>
       <b>less</b> <b>--help</b>
       <b>less</b> <b>-V</b>
       <b>less</b> <b>--version</b>
       <b>less</b> <b>[-[+]aABcCdeEfFgGiIJKLmMnNqQrRsSuUVwWX~]</b>
            <b>[-b</b> <u style="text-decoration-style:single">space</u><b>]</b> <b>[-h</b> <u style="text-decoration-style:single">lines</u><b>]</b> <b>[-j</b> <u style="text-decoration-style:single">line</u><b>]</b> <b>[-k</b> <u style="text-decoration-style:single">keyfile</u><b>]</b>
            <b>[-{oO}</b> <u style="text-decoration-style:single">logfile</u><b>]</b> <b>[-p</b> <u style="text-decoration-style:single">pattern</u><b>]</b> <b>[-P</b> <u style="text-decoration-style:single">prompt</u><b>]</b> <b>[-t</b> <u style="text-decoration-style:single">tag</u><b>]</b>
            <b>[-T</b> <u style="text-decoration-style:single">tagsfile</u><b>]</b> <b>[-x</b> <u style="text-decoration-style:single">tab</u><b>,...]</b> <b>[-y</b> <u style="text-decoration-style:single">lines</u><b>]</b> <b>[-[z]</b> <u style="text-decoration-style:single">lines</u><b>]</b>
            <b>[-#</b> <u style="text-decoration-style:single">shift</u><b>]</b> <b>[+[+]</b><u style="text-decoration-style:single">cmd</u><b>]</b> <b>[--]</b> <b>[</b><u style="text-decoration-style:single">filename</u><b>]...</b>
       (See  the  OPTIONS section for alternate option syntax with long option
       names.)

<b>DESCRIPTION</b>
       <u style="text-decoration-style:single">Less</u> is a program similar to <u style="text-decoration-style:single">more</u> (1), but it has many  more  features.
       <u style="text-decoration-style:single">Less</u>  does  not  have to read the entire input file before starting, so
       with large input files it starts up faster than text  editors  like  <u style="text-decoration-style:single">vi</u>
       (1).  <u style="text-decoration-style:single">Less</u> uses termcap (or terminfo on some systems), so it can run on
<span style="background-color:#FFFFFF"><font color="#300A24"> Manual page less(1) line 1 (press h for help or q to quit)</font></span>


<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ man more

MORE(1)                          User Commands                         MORE(1)

<b>NAME</b>
       more - file perusal filter for crt viewing

<b>SYNOPSIS</b>
       <b>more</b> [options] <u style="text-decoration-style:single">file</u>...

<b>DESCRIPTION</b>
       <b>more</b> is a filter for paging through text one screenful at a time.  This
       version is especially primitive.  Users  should  realize  that  <b>less</b>(1)
       provides <b>more</b>(1) emulation plus extensive enhancements.

<b>OPTIONS</b>
       Options are also taken from the environment variable <b>MORE</b> (make sure to
       precede them with a dash (<b>-</b>)) but command-line  options  will  override
       those.

       <b>-d</b>     Prompt  with &quot;[Press space to continue, &apos;q&apos; to quit.]&quot;, and dis‐
              play &quot;[Press &apos;h&apos; for instructions.]&quot; instead of ringing the bell
              when an illegal key is pressed.

       <b>-l</b>     Do not pause after any line containing a <b>^L</b> (form feed).
<span style="background-color:#FFFFFF"><font color="#300A24"> Manual page more(1) line 1 (press h for help or q to quit)</font></span>
</pre>

# Task 2  
**2a**. Discovering soft and hard links. Comment on results of these commands (place the output into your report):  
```
cd
mkdir test
cd test
touch test1.txt
echo “test1.txt” > test1.txt
ls -l . (a hard link)
ln test1.txt test2.txt
ls -l . // (pay attention to the number of links to test1.txt and test2.txt)
echo “test2.txt” > test2.txt
cat test1.txt test2.txt
rm test1.txt
ls -l . // (now a soft link)
ln -s test2.txt test3.txt
ls -l . //(pay attention to the number of links to the created files)
rm test2.txt; ls -l .
```

<pre><font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ cd # change the current/working directory to the home directory
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ mkdir test
mkdir: cannot create directory ‘test’: File exists
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~</b></font>$ cd test
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ touch test1.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ echo &quot;test1.txt&quot; &gt; test1.txt # write the line &quot;test1.txt&quot; to the `test1.txt` file
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls -l .
total 8
-rw-rw-r-- 1 vrnch vrnch   10 Nov 11 13:53 test1.txt
drwxrwxr-x 2 vrnch vrnch 4096 Nov 11 13:39 <font color="#3465A4"><b>test2</b></font>
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ln test1.txt test2.txt # create a link to the `test1.txt` file with name `test2.txt`
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls -l .
total 12
-rw-rw-r-- 2 vrnch vrnch   10 Nov 11 13:53 test1.txt
drwxrwxr-x 2 vrnch vrnch 4096 Nov 11 13:39 <font color="#3465A4"><b>test2</b></font>
-rw-rw-r-- 2 vrnch vrnch   10 Nov 11 13:53 test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ echo &quot;test2.txt&quot; &gt; test2.txt # write the line &quot;test2.txt&quot; to the `test2.txt` file
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ cat test1.txt test2.txt # concatenate the contents of the listed files and print them; the `test1.txt` file was rewritten as well, not only the `test2.txt`
test2.txt
test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ rm test1.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls -l .
total 8
drwxrwxr-x 2 vrnch vrnch 4096 Nov 11 13:39 <font color="#3465A4"><b>test2</b></font>
-rw-rw-r-- 1 vrnch vrnch   10 Nov 11 13:55 test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ln -s test2.txt test3.txt # create a symbolic link to the `test2.txt` file with name `test3.txt`
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ls -l .
total 8
drwxrwxr-x 2 vrnch vrnch 4096 Nov 11 13:39 <font color="#3465A4"><b>test2</b></font>
-rw-rw-r-- 1 vrnch vrnch   10 Nov 11 13:55 test2.txt
lrwxrwxrwx 1 vrnch vrnch    9 Nov 11 13:56 <font color="#06989A"><b>test3.txt</b></font> -&gt; test2.txt
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ rm test2.txt; ls -l . # the `test3.txt` link became broken
total 4
drwxrwxr-x 2 vrnch vrnch 4096 Nov 11 13:39 <font color="#3465A4"><b>test2</b></font>
lrwxrwxrwx 1 vrnch vrnch    9 Nov 11 13:56 <span style="background-color:#2E3436"><font color="#CC0000"><b>test3.txt</b></font></span> -&gt; <span style="background-color:#2E3436"><font color="#CC0000"><b>test2.txt</b></font></span>
</pre>

**2b**. Dealing with `chmod`.
An executable script. Open your favorite editor and put these lines into a file
```
#!/bin/bash
echo “Drugs are bad MKAY?”
```
Give name “script.sh” to the script and call to  
```chmod +x script.sh```  
Then you are ready to execute the script:  
```./script.sh```

<pre><font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ chmod +x script.sh
<font color="#4E9A06"><b>vrnch@vrnch-Swift-SF314-42</b></font>:<font color="#3465A4"><b>~/test</b></font>$ ./script.sh
Drugs are bad MKAY?
</pre>